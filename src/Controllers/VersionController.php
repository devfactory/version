<?php

namespace Devfactory\Version\Controllers;

use Illuminate\Support\Facades\Config;

class VersionController {

  /**
   * Get the version inside the file
   */
  public function get() {
    $filename = Config::get('version::version_file_name', false);

    if(!$filename){
      return;
    }

    $path = app_path() . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'version'. DIRECTORY_SEPARATOR. $filename;

    if(!file_exists($path)){
      return;
    }

    return file_get_contents($path);
  }
}