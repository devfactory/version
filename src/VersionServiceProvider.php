<?php namespace Devfactory\Version;

use Illuminate\Support\ServiceProvider;

class VersionServiceProvider extends ServiceProvider {

  /**
   * Indicates if loading of the provider is deferred.
   *
   * @var bool
   */
  protected $defer = false;

  /**
   * Bootstrap the application events.
   *
   * @return void
   */
  public function boot()
  {
    $this->package('devfactory/version', 'version', __DIR__);
  }

  /**
   * Register the service provider.
   *
   * @return void
   */
  public function register()
  {
    //
    $this->app['version'] = $this->app->share(function($app) {
      return new Controllers\VersionController();
    });

  }

  /**
   * Get the services provided by the provider.
   *
   * @return array
   */
  public function provides()
  {
    return array('version');
  }

}
