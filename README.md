##How to setup

update `composer.json` file:

```json
{
    "require": {
        "laravel/framework": "4.1.*",
        "devfactory/version": "dev-master"
    }
}
```

and run `composer update` from terminal to download files.

update `app.php` file in `app/config` directory:

```php
'providers' => array(
    'Devfactory\Version\VersionServiceProvider',
),
```

```php
alias => array(
    'Version'         => 'Devfactory\Version\VersionFacadeProvider',
),
```

## Publish the configuration
```bash
php artisan config:publish devfactory/version
```

##How to use
in the config put the name of the version file:
for example : ch.devfactory.project.webapp.version	

in your HTML form add following code:

```html
{{ Version::get() }}

```
